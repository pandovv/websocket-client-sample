package pandov.websocketclient

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
//import com.neovisionaries.ws.client.WebSocket
//import com.neovisionaries.ws.client.WebSocketFactory
import kotlinx.android.synthetic.main.activity_main.*
import pandov.websocketclient.stomp.Stomp
import pandov.websocketclient.stomp.client.StompClient
import io.reactivex.CompletableTransformer


class MainActivity : AppCompatActivity() {

    companion object {
        private const val WS_URL = "ws://192.168.56.1:8080/gs-guide-websocket"
        private const val CONNECT_TIMEOUT = 5000
    }

    //    private var webSocket: WebSocket? = null
    private var stompClient: StompClient? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onDestroy() {
        super.onDestroy()
        disconnect()
    }

    private fun connect() {
//        val factory = WebSocketFactory()
//        webSocket = factory.createSocket(WS_URL, CONNECT_TIMEOUT)
//        webSocket?.addListener(MyWebSocketAdapter())
//        webSocket?.connect()
        stompClient = Stomp.over(Stomp.ConnectionProvider.OKHTTP, WS_URL)
        stompClient?.connect()
        stompClient?.topic("/topic/greetings")
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe {
                    Log.e("TEST", it.payload)
                    showGreeting(it.payload)
                }
    }

    private fun disconnect() {
//        webSocket?.disconnect()
        stompClient?.disconnect()
    }

    private fun showGreeting(message: String) {
        text_greetings.append("\n$message")
    }

    private fun sendName(message: String) {
        stompClient?.send("/app/hello", "{ \"name\": \"$message\" }")
                ?.compose {
                    it.unsubscribeOn(Schedulers.newThread()).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                }
                ?.subscribe(
                        { Log.d("TEST", "STOMP echo send successfully") },
                        { Log.e("TEST", "Error send STOMP echo $it") }
                )
    }

    fun onClickConnect(view: View) {
        connect()
        but_connect.isEnabled = false
        but_disconnect.isEnabled = true
    }

    fun onClickDisconnect(view: View) {
        disconnect()
        but_connect.isEnabled = true
        but_disconnect.isEnabled = false
    }

    fun onClickSend(view: View) {
        sendName(input_name.text.toString())
        input_name.text.clear()
    }
}
