package pandov.websocketclient

import android.util.Log
import com.neovisionaries.ws.client.WebSocket
import com.neovisionaries.ws.client.WebSocketAdapter

class MyWebSocketAdapter : WebSocketAdapter() {

    override fun onTextMessage(websocket: WebSocket?, text: String?) {
        Log.d("onTextMessage", text)
    }

}